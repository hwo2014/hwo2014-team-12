// INCLUDES
var net = require("net");
var fs = require('fs');
var JSONStream = require('JSONStream');

// SERVER CONNECTION VARS
var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];
var botTrack = process.argv[6];
var botNumPlayers = process.argv[7];

// RACE INIT
var displayedName = false;
var racetrack = null;
var currentThrottle = 0.5;
var prevTrack = -1;
var currTrack = 0;
var currentTrack;
var inBendDistance = 0;
var nextAngle = 0;
var nextTurnAngle = 0;
var nextTurnRadius = 100;
var prevNextAngle = -1;
var prevNextTotalAngle = -1;
var prevCarAngle = 0;
var prevInPieceDistance = 0;
var prevTime = 0;
var speed = 0;
var prevSpeed = 0;
var realSpeed = 0;
var myCar;
var myCarColor = "";
var highestCarAngle = 0;
var raceStarted = 0;
var trackName = "";
var turboAvailable = 0;
var inTurbo = 0;
var aveSpeed = 0;

// TOGGLE LOGGING
var debugLog = 0;
var debugLogFileName = "Log";
var visualizerFileName = "Viz";
var d = new Date();
prevTime = d.getTime();

debugLogFileName = debugLogFileName + "-" + prevTime + ".txt";
visualizerFileName = visualizerFileName + "-" + prevTime + ".html";

// SERVER CONNECTION
console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);



client = net.connect(serverPort, serverHost, function() {

    if (debugLog == 0)
    {
      return send({
        msgType: "join",
        data: {
          name: botName,
          key: botKey
        }
      });
    }
    else
    {
      if (botTrack === undefined || botTrack == null || botTrack == "") { botTrack = "keimola"; }
      if (botNumPlayers === undefined || botNumPlayers == null || botNumPlayers == "") { botNumPlayers = 1; }
      return send({"msgType": "joinRace", "data": {
        "botId": {
          "name": botName,
          "key": botKey
        },
        "trackName": botTrack, // keimola
        //"password": "schumi4ever",
        "carCount": botNumPlayers
      }});  
    }
});


// METHODS
function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};


function logNum(n) {
	return parseFloat(Math.round(n * 100) / 100).toFixed(3);
}
function tableCell(n, idname) {
  return "<td class=\""+idname+"\">"+n+"</td>";
}
function getNextAngle(currentPos) {
  for (var i = currentPos+1; i < racetrack.pieces.length - 1; i++) {
    if (racetrack.pieces[i].angle === undefined) {
    } else {
      return racetrack.pieces[i].angle;
    }
  }
  return 0;
}

// Get distance to the next turn
function getDistanceToTurn(currentPos) {
  var distanceToTurn = 0;
  if (currentPos == racetrack.pieces.length - 1) { currentPos = -1; }
  var i = currentPos+1;
  while (i < racetrack.pieces.length) {
    if (racetrack.pieces[i].angle === undefined) {
      distanceToTurn += racetrack.pieces[i]["length"];
    } else {
      nextTurnAngle = racetrack.pieces[i].angle;
      nextTurnRadius = racetrack.pieces[i].radius;
      return distanceToTurn;
    }
    if (i == racetrack.pieces.length - 1) { i = -1; }
    i++;
  }
  return distanceToTurn;
}

// GEt distance to the next striaght path
function getDistanceToStraight(currentPos) {
  var distanceToStraight = 0;
  if (currentPos == racetrack.pieces.length - 1) { currentPos = -1; }
  var i = currentPos+1;
  while (i < racetrack.pieces.length) {
    if (racetrack.pieces[i].angle === undefined) {
      return distanceToStraight;
    } else {
      if (racetrack.pieces[i].angle * racetrack.pieces[currentPos].angle < 0) return distanceToStraight;
      distanceToStraight += (racetrack.pieces[i].radius * 2 * 3.14159) * (Math.abs(racetrack.pieces[i].angle) / 360);
    }
    if (i == racetrack.pieces.length - 1) { i = -1; }
    i++;
  }
  return distanceToStraight;
}
function getDistanceToStraightBack(currentPos) {
  var distanceToStraight = 0;
  if (currentPos == 0) { currentPos = racetrack.pieces.length; }
  var i = currentPos-1;
  while (i > 0) {
    if (racetrack.pieces[i].angle === undefined) {
      return distanceToStraight;
    } else {
      if (racetrack.pieces[i].angle * racetrack.pieces[currentPos].angle < 0) return distanceToStraight;
      distanceToStraight += (racetrack.pieces[i].radius * 2 * 3.14159) * (Math.abs(racetrack.pieces[i].angle) / 360);
    }
    if (i == 0) { i = racetrack.pieces.length+1; }
    i--;
  }
  return distanceToStraight;
}
function getMyDistanceInBend(currentPos) {
  var forwardDistance = getDistanceToStraight(currentPos);
  var backDistance = getDistanceToStraightBack(currentPos);
  backDistance += myCar.piecePosition.inPieceDistance;
  forwardDistance += (((currentTrack.radius * 2 * 3.14159) * (Math.abs(currentTrack.angle) / 360)) - myCar.piecePosition.inPieceDistance);
  return (backDistance * 100) / (backDistance + forwardDistance);
}

function getDistanceToStraight_OLD(currentPos) {
  var distanceToStraight = 0;
  if (currentPos == racetrack.pieces.length - 1) { currentPos = -1; }
  for (var i = currentPos+1; i < racetrack.pieces.length - 1; i++) {
    if (racetrack.pieces[i].angle === undefined) {
      return distanceToStraight;
    } else {
      distanceToStraight += (racetrack.pieces[i].radius * 2 * 3.14159) * (Math.abs(racetrack.pieces[i].angle) / 360);
    }
    if (i == racetrack.pieces.length - 1) { i = -1; }
  }
  return distanceToStraight;
}


function getNextBendTotalAngle(currentPos) {
  if (currentPos == racetrack.pieces.length - 1) { currentPos = -1; }
  var i = currentPos+1;
  var startTrack = -1; 
  var endTrack = -1;
  var totalAngle = 0;
  while (i < racetrack.pieces.length) {

      if (startTrack != -1) {
        if (racetrack.pieces[i].angle === undefined) {
        } else {
          totalAngle += racetrack.pieces[i].angle * (racetrack.pieces[i].radius / 100); 
        }
      }

    if (racetrack.pieces[i]["switch"] === undefined) {
    } else {
      if (startTrack == -1) { startTrack = i; } else { endTrack = i; }  
    }

    if (startTrack != -1 && endTrack != -1) { 
      return totalAngle; 
    }
    if (i == racetrack.pieces.length - 1) { i = -1; }    
    i++;
  }
  return totalAngle;
}

function getNextTotalAngle(currentPos) {
  if (currentPos == racetrack.pieces.length - 1) { currentPos = -1; }
  var i = currentPos+1;
  var startTrack = -1; 
  var endTrack = -1;
  var totalAngle = 0;
  while (i < racetrack.pieces.length) {

      if (startTrack != -1) {
        if (racetrack.pieces[i].angle === undefined) {
        } else {
          totalAngle += racetrack.pieces[i].angle * (racetrack.pieces[i].radius / 100); 
        }
      }

    if (racetrack.pieces[i]["switch"] === undefined) {
    } else {
      if (startTrack == -1) { startTrack = i; } else { endTrack = i; }  
    }

    if (startTrack != -1 && endTrack != -1) { 
      return totalAngle; 
    }
    if (i == racetrack.pieces.length - 1) { i = -1; }    
    i++;
  }
  return totalAngle;
}

if (debugLog == 1) {
  // Create blank log file
  fs.writeFile(debugLogFileName, "", function(err) { if(err) { console.log(err); } });
  //fs.writeFile("data.csv", "", function(err) { if(err) { console.log(err); } });
  fs.createReadStream('html-template.txt').pipe(fs.createWriteStream(visualizerFileName));
}

jsonStream = client.pipe(JSONStream.parse());

// SERVER MESSAGE EVENT
jsonStream.on('data', function(data) {

  if (data.msgType === 'carPositions') {

    for (var ci = 0; ci < data.data.length; ci++)
    {
      if(data.data[ci].id.color == myCarColor)
      {
        myCar = data.data[ci]; 
      }
    }

    if (displayedName == false) {
      console.log('Name: ' + myCar.id.name + ' (' + myCar.id.color + ')');
      displayedName = true;
    }
    var gameTick = data.gameTick;
    var commandSent = false;


    if (highestCarAngle < Math.abs(myCar.angle)) {
      highestCarAngle = Math.abs(myCar.angle);
    }

    // Which track piece are we on?
    prevTrack = currTrack;
    currentTrack = racetrack.pieces[myCar.piecePosition.pieceIndex];
    currTrack = myCar.piecePosition.pieceIndex;
    if (prevTrack != currTrack) {
      console.log('Piece ' + currTrack);
      if (debugLog == 1)
      {
        fs.appendFile(visualizerFileName, "<tr class=\"sep\"></tr>",  function (err) { });
      }
    }

    var d = new Date();
    currTime = d.getTime();
    passedTime = currTime - prevTime;
    // This logs our speed at units per tick?
    prevSpeed = speed;
    if (myCar.piecePosition.inPieceDistance > prevInPieceDistance)
    {
      speed = myCar.piecePosition.inPieceDistance - prevInPieceDistance;
      realSpeed = (myCar.piecePosition.inPieceDistance - prevInPieceDistance) * 1000 / passedTime;
      
    }

    prevTime = currTime;

    var distanceToTurn = (currentTrack.length - myCar.piecePosition.inPieceDistance) + getDistanceToTurn(myCar.piecePosition.pieceIndex);
	  var distanceToStraight = (((currentTrack.radius * 2 * 3.14159) * (Math.abs(currentTrack.angle) / 360)) - myCar.piecePosition.inPieceDistance) + getDistanceToStraight(myCar.piecePosition.pieceIndex);

    maxSpeed = 10;
    if (inTurbo == 1)
    {
      maxSpeed = 13;
    }
  
    if (currentTrack.radius === undefined)
    { 
    ////////////////////////////////////////  
    // LOGIC FOR STRAIGHT TRACKS
    ////////////////////////////////////////      
    currentThrottle = 1;

    targetSpeed = 7.4;
    if (trackName == "USA") {
      targetSpeed = 9.7;
    }
    if (trackName == "Germany") {
      targetSpeed = 5;
    }
    if (trackName == "France") {
      targetSpeed = 4;
    }
    
    distanceNeeded = 0;
    if (speed < targetSpeed)
    {
      aveAcc = ((targetSpeed + speed) / 80);
      distanceNeeded = ((targetSpeed * targetSpeed) - (speed * speed)) / (2 * aveAcc);
    }
    if (speed > targetSpeed)
    {
      aveDec = ((maxSpeed - targetSpeed + maxSpeed - speed) / 90);
      distanceNeeded = ((speed * speed) - (targetSpeed * targetSpeed)) / (2 * aveDec);
    }

    //if (speed < targetSpeed && distanceToTurn - speed < distanceNeeded)
    //{
    //  currentThrottle = ((targetSpeed * 1.02) + ((targetSpeed - speed) * 25) ) / 10;
    //}

    if (speed > targetSpeed && distanceToTurn - speed < distanceNeeded)
    {
      currentThrottle = ((targetSpeed * 1.02) - ((speed - targetSpeed) * 25) ) / 10;
    }



        if (turboAvailable == 1 && myCar.piecePosition.lap > 0 && distanceToTurn > 380) {
            switchedLane = true;
            turboAvailable = 0;
            send({
              msgType: "turbo",
              data: "Turbo Engaged!"
            });        
            if (debugLog == 1) { console.log("Turbo Engaged");
              fs.appendFile(debugLogFileName, "Turbo Engaged \r\n\r\n", function (err) { });
            }
        }

    }
    else 
    {
    ////////////////////////////////////////  
    // LOGIC FOR BENDS 
    ////////////////////////////////////////  
      inBendDistance = getMyDistanceInBend(myCar.piecePosition.pieceIndex);


      if (inBendDistance < 53)    
      {
        targetSpeed = 5.5;
        if (trackName == "USA") {
          targetSpeed = 8.2;
        }
        if (trackName == "Germany") {
          targetSpeed = 4;
        }
        if (trackName == "France") {
          targetSpeed = 4;
        }
        
        if (speed < targetSpeed)
        {
          currentThrottle = ((targetSpeed * 1.02) + ((targetSpeed - speed) * 25) ) / 10;
        }
        if (speed > targetSpeed)
        {
          currentThrottle = ((targetSpeed * 1.02) - ((speed - targetSpeed) * 25) ) / 10;
        }
      }
      else
      {
        targetSpeed = 10;
        
        if (speed < targetSpeed)
        {
          currentThrottle = ((targetSpeed * 1.02) + ((targetSpeed - speed) * 25) ) / 10;
        }
        if (speed > targetSpeed)
        {
          currentThrottle = ((targetSpeed * 1.02) - ((speed - targetSpeed) * 25) ) / 10;
        }        
      }

    }

    // Set throttle boundaries to prevent errors
    if (currentThrottle < 0) { currentThrottle = 0.000; }
    if (currentThrottle > 1) { currentThrottle = 1; }


    ////////////////////////////////////////  
    // LOGGING
    ////////////////////////////////////////  
    if (debugLog == 1) {
      fs.appendFile(debugLogFileName, gameTick + " | " + logNum(currentThrottle)  + " | " + logNum(speed) + " | " + logNum(speed - prevSpeed) + " | " + logNum(realSpeed) + " | " + logNum(inBendDistance) + " | "+ logNum(distanceToTurn) + " | " + logNum(distanceToStraight) + " | " + JSON.stringify(myCar) + "\r\n\r\n", function (err) { });
      fs.appendFile(visualizerFileName, "<tr>"+tableCell(gameTick,"tick")+tableCell(myCar.piecePosition.pieceIndex,"trackn") + tableCell(logNum(currentThrottle*10),"throt") + tableCell(logNum(speed),"speed") + tableCell(logNum(myCar.angle),"angle") + tableCell(logNum(inBendDistance),"ibd") +  tableCell(logNum(distanceToTurn),"dtt") +  tableCell(logNum(distanceToStraight),"dts") + "</tr>\r\n",  function (err) { });
    }


    // LANE SWITCH LOGIC
    prevCarAngle = myCar.angle;
    nextAngle = getNextAngle(currTrack);


    if (trackName == "USA" && gameTick == 100) {
          console.log('switching to Right');
          commandSent = true;
          send({
            msgType: "switchLane",
            data: "Right"
          });

    }

    // Calculate if we need to switch lanes at the next bend
    nextTotalAngle = getNextTotalAngle(currTrack);    
    if (prevNextTotalAngle != nextTotalAngle && raceStarted == 1 && gameTick > 5) {
      if (debugLog == 1) { console.log('next total Angle ' + nextTotalAngle); }
        if (nextTotalAngle > 0) {
          console.log('switching to Right');
          commandSent = true;
          send({
            msgType: "switchLane",
            data: "Right"
          });
        }
        if (nextTotalAngle < 0) {
          console.log('switching to Left');
          commandSent = true;
          send({
            msgType: "switchLane",
            data: "Left"
          });
        }
      //}
      prevNextTotalAngle = nextTotalAngle;
    }
    prevNextAngle = nextAngle;
    if (commandSent == false)
    {
        send({
          msgType: "throttle",
          data: currentThrottle
        });
    }

    // SAVING PREVIOUS STATE
    
    prevInPieceDistance = myCar.piecePosition.inPieceDistance;
    process.stdout.write(".");

  } else {
    if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameInit') {
      console.log('Game Initialize');
      console.log('Track Name: '+ data.data.race.track.name);
      trackName = data.data.race.track.name;
      console.log('Pieces: '+ data.data.race.track.pieces.length);
      racetrack = data.data.race.track;
      if (debugLog == 1) {
        fs.writeFile("track.txt", JSON.stringify(racetrack), function(err) {if(err) {console.log(err);}});
      }
    } else if (data.msgType === 'turboAvailable') {
      console.log('Turbo Available');
      turboAvailable = 1;
    } if (data.msgType === 'turboStart') {
        if (myCarColor == data.data.color)
        {
          console.log('Turbo Start');
          inTurbo = 1;
        }
    } if (data.msgType === 'turboEnd') {
      if (myCarColor == data.data.color)
        {
          console.log('Turbo End');
          inTurbo = 0;
        }
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
      raceStarted = 1;
    } else if (data.msgType === 'lapFinished') {
      if (debugLog == 1) {
        // log all other events
        fs.appendFile(debugLogFileName, data.msgType + " | " + JSON.stringify(data.data) + "\r\n\r\n", function (err) { });
        console.log(data.msgType + " | " + JSON.stringify(data.data) );
      }
      nextTotalAngle = 0;
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
      if (debugLog == 1) { console.log('Highest Car Angle ' + highestCarAngle); 
        fs.appendFile(visualizerFileName, "</table></body</html>",  function (err) { });
       }
    } else if (data.msgType === 'yourCar') {
      console.log('My Car ' + JSON.stringify(data.data));
      myCarColor = data.data.color;
    } else {
      if (debugLog == 1) {
        // log all other events
        fs.appendFile(debugLogFileName, data.msgType + " | " + JSON.stringify(data.data) + "\r\n\r\n", function (err) { });
        console.log(data.msgType + " | " + JSON.stringify(data.data) );
      }
    }
    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
